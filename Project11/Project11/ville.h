#pragma once
#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
#include <vector>
#include "famille.h"
#include "quartier.h"

using namespace std;

class ville
{


private:
	string nom;
	double revenu;
	double pourc_p;
	vector <quartier> lq;
	double min;
	double max;


public:

	ville(string nom,string fichier) {

		this->nom = nom;

	

		lire_donnes(fichier);
		
		calcul_revenu();
		calcul_pourc_p();


	}



	string getnom() {
		return nom;
	}
	double getrevenu() {
		return round(revenu);
	}
	double getpourc_p() {
		return round(pourc_p);
	}
	double getmax() {
		return max;
	}
	double getmin() {
		return min;
	}
	vector <quartier> quartiers() {
		return this->lq;
	}


	void calcul_revenu() {

		double somme = 0;

		max = lq[0].getrevenu();
		min = lq[0].getrevenu();

		
		for (quartier f : lq) {

			if (max < f.getrevenu())
				max = f.getrevenu();

			if (min > f.getrevenu())
				min = f.getrevenu();

			somme += f.getrevenu();

		}
		this->revenu = somme / lq.size();

	}
	void calcul_pourc_p() {
		double p = 0;

		for (quartier q : lq) {


			p = p + q.getpourc_p();


		}

		this->pourc_p =(double)p/(double)lq.size();

	}
	vector <quartier> quartiers_defav() {

		vector <quartier> local;

		for (quartier q : lq) {
			if (this->revenu > q.getrevenu())
				local.push_back(q);

		}

		return local;
	}
	vector <quartier> qmax() {

		vector <quartier> local;

		for (quartier q : lq) {
			if (this->max == q.getrevenu())
				local.push_back(q);

		}

		return local;

	}
	vector <quartier> qmin() {

		vector <quartier> local;

		for (quartier q : lq) {
			if (this->min == q.getrevenu())
				local.push_back(q);

		}

		return local;


	}


	


	void lire_donnes(string fichier) {

		
		string opt;
		ifstream donnes(fichier);

		if (donnes.is_open())
		{
			string nomq = "";
		
			while (!donnes.eof()) {


				vector <famille> local;

				

			    getline(donnes, opt);				
				if(isalpha(opt[0]))
				 nomq=opt;

				while (!isalpha(opt[0]) && opt != ";") {

					

				string id =opt.substr(0,opt.find("	") + 1);
				opt= opt.substr(opt.find("	") + 1);
				string revenu = opt.substr(0, opt.find("	") + 1);
				opt = opt.substr(opt.find("	") +1);
				string nombre = opt;


					famille f(stoi(id),stod(revenu),stoi(nombre));
					local.push_back(f);
					
					
					getline(donnes, opt);
				}


				if (local.size() > 0) {
					quartier q(nomq, local);
					this->lq.push_back(q);
				}
				


				
				


			}
			donnes.close();
		}

		
	}

	string analyse(string fichier) {

		

			ofstream donnes;
			donnes.open(fichier);
			

			donnes << "Revenu Moyen: " << getrevenu() << endl;

			donnes << "Liste des quartiers defavorables: " << endl;
			for (quartier q : quartiers_defav()) {

				donnes << q.getnom() << " Revenu moyen: " << round(q.getrevenu()) << endl;
			}


			donnes << "Quartiers avec le plus petit revenu moyen: " << endl;
			for (quartier q : qmin()) {

				donnes << q.getnom() << " Revenu moyen: " << round(q.getrevenu()) << endl;
			}


			donnes << "Quartiers avec le plus grand revenu moyen: " << endl;
			for (quartier q : qmax()) {

				donnes << q.getnom() << " Revenu moyen: " << round(q.getrevenu()) << endl;
			}


			donnes << "Pourcentage de menages dont le revenu est faible : " << getpourc_p() << "%" << endl;


			return "Donnees Televerses !";

	}



};




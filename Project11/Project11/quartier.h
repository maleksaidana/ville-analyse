#pragma once
#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
#include <vector>
#include "famille.h"

class quartier
{

private:
	string nom;
	double revenu;
	double pourc_p;
	vector <famille> lf;
	


public:

	quartier(string nom, vector <famille> lf) {

		this->nom = nom;
		this->lf = lf;
		
		
	
		calcul_revenu();
		calcul_pourc_p();


	}


	string getnom() {
		return nom;
	}
	double getrevenu() {
		return revenu;
	}
	double getpourc_p() {
		return pourc_p;
	}
	vector <famille> familles() {
		return this->lf;
	}
	


	void calcul_revenu() {

		double somme = 0;
		for (famille f : lf) {

			

			somme+= f.getrevenu();

		}
		this->revenu = somme / lf.size();

	}

	void calcul_pourc_p() {
		int p=0;

		for (famille f : lf) {

			if (f.getpauvre()) {
				p++;
			}
			
		

		}
		
		this->pourc_p = ((double)p/(double)lf.size())*100 ;

	}



	
};



